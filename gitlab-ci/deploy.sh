#!/bin/bash
#faire arreter l'execution en cas d'erreur
set -e

#copy to release directory
mkdir -p /appli/$1/releases

appdirectory="$1_$(date '+%Y-%m-%d_%H-%M-%S')"

cp -R . /appli/$1/releases/$appdirectory

#lien symbolic 
echo "Linking"
if [ -L "/appli/$1/current" ]; then
  rm /appli/$1/current
fi
ln -s /appli/$1/releases/$appdirectory /appli/$1/current

chmod +x /appli/$1/current/script/testAndAlert.sh

#cleaning of old releases, we only keep the last 5
for oldreleases in $(ls -t "/appli/$1/releases" | tail -n +6)
do
rm -rf "/appli/$1/$2/releases/$oldreleases"
done 

#00 07 * * 1-5 /appli/testAndAlert/current/script/testAndAlert.sh visio.pixees.fr
#00 07 * * 1-5 /appli/testAndAlert/current/script/testAndAlert.sh live.pixees.fr
#00 07 * * 1-5 /appli/testAndAlert/current/script/testAndAlert.sh coturn.pixees.fr

