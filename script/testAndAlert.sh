#!/bin/bash
#usage : 
#* 7-20 * * 1-5 /root/testAndAlert/script/testAndAlert.sh visio.pixees.fr
#* 7-20 * * 1-5 /root/testAndAlert/script/testAndAlert.sh coturn.pixees.fr

#on test si le serveur renvoie bien un 200
if curl -LI $1 -o /dev/null -w '%{http_code}\n' -s --connect-timeout 3 | grep "200" > /dev/null; 
then
  exit
else
  #sinon on envoie un mail
  echo "Subject: LE SERVEUR $1 NE REPOND PAS" | /usr/sbin/ssmtp "$2";
fi


